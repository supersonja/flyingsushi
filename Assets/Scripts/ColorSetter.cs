﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Sets the color of a <see cref="Graphic"/>.
/// </summary>
public class ColorSetter : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The grafic to target.")]
    protected Graphic graphic = null;

    [SerializeField, Tooltip("The color to set.")]
    protected Color color = Color.white;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Sets the saved color.
    /// </summary>
    public void SetColor()
    {
        graphic.color = color;
    }
}
