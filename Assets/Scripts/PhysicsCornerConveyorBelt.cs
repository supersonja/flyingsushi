﻿using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsCornerConveyorBelt : MonoBehaviour
{
    [SerializeField]
    private float beltAngularSpeed = 20f;

    private new Rigidbody rigidbody;
    private Quaternion original;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Vector3 start = transform.position + transform.up * 0.025f;
        for (float angle = 0f; beltAngularSpeed > 0f && angle < 90f; angle += beltAngularSpeed)
        {
            Vector3 end;
            Vector3 center = transform.position + transform.up * 0.025f + (transform.right * Mathf.Sin((angle + beltAngularSpeed) * Mathf.Deg2Rad) - transform.forward * Mathf.Cos((angle + beltAngularSpeed) * Mathf.Deg2Rad)) * 0.5f;
            if (angle < 45f)
            {
                end = transform.position + transform.up * 0.025f + transform.right * Mathf.Tan(angle * Mathf.Deg2Rad) - transform.forward;
            }
            else if (angle > 45f)
            {
                end = transform.position + transform.up * 0.025f + transform.right - transform.forward * (Mathf.Tan((90f - angle) * Mathf.Deg2Rad));
            }
            else
            {
                end = transform.position + transform.up * 0.025f + transform.right - transform.forward;
            }
            Gizmos.DrawLine(start, center);
            Gizmos.DrawLine(center, end);
        }
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        original = rigidbody.rotation;
    }

    private void FixedUpdate()
    {
        // Calculate the belts movement.
        Vector3 movement = transform.up * beltAngularSpeed * Time.fixedDeltaTime * transform.localScale.x * transform.localScale.z;
        // Move the belt back without pyhsics.
        rigidbody.rotation = Quaternion.Euler(transform.eulerAngles + movement);
        // Move the belt back with physics, so it will move all rigidbodys on top.
        rigidbody.MoveRotation(original);
    }
}
