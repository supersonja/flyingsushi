﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Sprite sprite;

    [SerializeField]
    private Color activeColor = Color.white;
    [SerializeField]
    private Color inactiveColor = new Color(0.5f, 0.5f, 0.5f, 0.5f);

    [SerializeField, Range(0f, 180f)]
    private float angle = 10f;

    private List<Image> display = new List<Image>();

    private RectTransform RectTransform
    {
        get
        {
            return (RectTransform)transform;
        }
    }

    public void UpdateMax(int max)
    {
        bool updatePosition = false;
        while (display.Count < max)
        {
            display.Add(AddDisplay(display.Count + 1, max));
            updatePosition = true;
        }
        while (display.Count > max)
        {
            Destroy(display[display.Count - 1].gameObject);
            display.RemoveAt(display.Count - 1);
            updatePosition = true;
        }

        if (updatePosition)
        {
            UpdatePosition();
        }
    }

    public void DisplayHealth(int amount)
    {
        for (int i = 0; i < display.Count; i++)
        {
            display[i].color = i < amount ? activeColor : inactiveColor;
        }
    }

    private Image AddDisplay(int number, int max)
    {
        Image display = new GameObject("Display").AddComponent<Image>();
        display.transform.SetParent(transform);
        float size = RectTransform.sizeDelta.x / (max * 2f + 1f);
        display.transform.localPosition = Vector2.right * size * (number * 2f + 1f);
        display.transform.eulerAngles = Vector3.forward * Random.Range(-angle, angle);
        ((RectTransform)display.transform).sizeDelta = Vector2.one * RectTransform.sizeDelta.y;
        display.sprite = sprite;
        return display;
    }

    private void UpdatePosition()
    {
        float size = RectTransform.sizeDelta.x / (display.Count * 2f);
        for (int number = 0; number < display.Count; number++)
        {
            display[number].transform.localPosition = Vector2.right * size * (number * 2f + 1);
        }
    }
}
