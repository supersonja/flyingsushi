﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleManager : MonoBehaviour                      //manages who orders when 
{
    [SerializeField]
    private List<Customer> free = new List<Customer>();         //customers free to order
    [SerializeField]
    private Food[] allFoods;
    private List<Customer> ordering = new List<Customer>();
    [SerializeField]
    private float mintime = 6f;
    [SerializeField]
    private float maxtime = 20f;
    [SerializeField]
    private float patience = 10f;                               //time for order

    private float currtime = 0;
    private int minFoodCount = 1;
    private int maxFoodCount = 4;

    void Update()
    {   
        //if time is right chooses a random Customer to do the next order, removes from free list, calls order with random amount of food 
        //and time for order and sets new timer for when the next order should start
        currtime -= Time.deltaTime;
        if (currtime <= 0 && free.Count != 0)
        {
            Customer customer = free[Random.Range(0, free.Count)];
            free.Remove(customer);
            ordering.Add(customer);
            customer.Order(GetRandomFoods(), patience);
            currtime = Random.Range(mintime, maxtime);
        }
    }

    //changes customer from ordering to free list
    public void SetCustomerFree(Customer customer)
    {
        ordering.Remove(customer);
        free.Add(customer);
    }

    //get an array with a random number of fooditems between min and max from the list of all foods
    private Food[] GetRandomFoods()
    {
        Food[] order = new Food[Random.Range(minFoodCount, maxFoodCount)];

        for (int i = 0; i < order.Length; i++)
        {
            order[i] = allFoods[Random.Range(0, allFoods.Length)];
        }

        return order;
    }
}
