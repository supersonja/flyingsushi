﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetOut : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if(other.attachedRigidbody && other.tag == "food")
        {
            other.attachedRigidbody.transform.position += Vector3.up * (transform.position.y - other.attachedRigidbody.transform.position.y);
        }

    }
}
