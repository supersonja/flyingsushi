﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeDisplay : MonoBehaviour
{
    [SerializeField]
    private Transform[] anchors;

    [SerializeField]
    private LineRenderer renderer;


    public void DisplayCode(List<int> code)
    {
        renderer.positionCount = code.Count;
        for (int i = 0; i < code.Count; i++)
        {
            renderer.SetPosition(i, anchors[code[i]].position);
        }
    }
}
