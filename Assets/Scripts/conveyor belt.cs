﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> spawnableFood = new List<GameObject>();
    [SerializeField]
    private float foodspeed = 1f;
    [SerializeField]
    private float spawntime = 3f;
    [SerializeField]
    private Vector3[] waypoints = new Vector3[0];

    private List<MovingFood> foodOnBelt = new List<MovingFood>();

    private float currentTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // spawntime update

        if (currentTime <= 0)
        {
            currentTime = spawntime;
            SpawnFood();
        }
        currentTime -= Time.deltaTime;


        //move food
        float step = foodspeed * Time.deltaTime;
        foreach(MovingFood food in foodOnBelt)
        {
            Vector3 direction = waypoints[food.targetPoint] -food.food.position;
            if(direction.sqrMagnitude < step*step)
            {

                if(food.targetPoint < waypoints.Length - 1)
                {
                    food.food.position = waypoints[food.targetPoint];
                    food.targetPoint++;
                }
                else
                {
                    Destroy(food.food.gameObject);
                    //todo: food aus der liste löschen
                }
              
            }
        }


        
    }

    private void SpawnFood()
    {
        GameObject fooditem = spawnableFood[Random.Range(0,spawnableFood.Count)];
        Transform spawnedFood = Instantiate(fooditem, waypoints[0],Quaternion.identity).transform;
        foodOnBelt.Add(new MovingFood(spawnedFood,1));
    }


    private class MovingFood
    {
        public Transform food;
        public int targetPoint;

        public MovingFood(Transform food, int targetPoint)
        {
            this.food = food;
            this.targetPoint = targetPoint;

        }
    }
}
