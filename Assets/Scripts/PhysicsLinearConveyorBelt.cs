﻿using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsLinearConveyorBelt : MonoBehaviour
{
    [SerializeField]
    private float beltSpeed = 0.3f;

    private new Rigidbody rigidbody;
    private Vector3 original;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Vector3 start = transform.position + transform.up * 0.025f - transform.forward;
        for (float i = 2f; beltSpeed > 0f && i >= beltSpeed; i -= beltSpeed)
        {
            Gizmos.DrawLine(start + transform.forward * i, start + transform.forward * (i - beltSpeed) + transform.right * 0.5f);
            Gizmos.DrawLine(start + transform.forward * i, start + transform.forward * (i - beltSpeed) - transform.right * 0.5f);
        }
    }

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        original = rigidbody.position;
    }

    private void FixedUpdate()
    {
        // Calculate the belts movement.
        Vector3 movement = transform.forward * beltSpeed * Time.fixedDeltaTime;
        // Move the belt back without pyhsics.
        rigidbody.position -= movement;
        // Move the belt back with physics, so it will move all rigidbodys on top.
        rigidbody.MovePosition(original);
    }
}
