﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RespawnArea : MonoBehaviour
{

    [SerializeField]
    float selecttime = 1;
    bool inArea = false;
    float currTime = 0;
    [SerializeField]
    private UnityEvent OnRestart;
    // Start is called before the first frame update
    [SerializeField]
    private FloatEvent onTimeUpdate;

    void Start()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        inArea = true;
    }
    private void OnTriggerExit(Collider other)
    {
        inArea = false;
        currTime = 0;
        onTimeUpdate.Invoke(0);
    }
    // Update is called once per frame
    void Update()
    {
        if (inArea)
        {
            currTime += Time.deltaTime;
            onTimeUpdate.Invoke(currTime/selecttime);

            if (currTime >= selecttime)
            {
                OnRestart.Invoke();
                inArea = false;
            }
        }

    }

    [System.Serializable]
    private class FloatEvent : UnityEvent<float> { }
}
