﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodDisplay : MonoBehaviour
{
    [SerializeField]
    private Image image;
    private bool isDone = false;

    public bool IsDone
    {
        get { return isDone; }
    }
    private RectTransform RectTransform
    {
        get
        {
            return (RectTransform)transform;
        }
    }

    //updates whats shown in the bubble, greys out recieved parts of order, sets isDone for item in list
    public void SetDone()
    {
        Color imagealpha = image.color;
        imagealpha.a = 0.3f;
        image.color = imagealpha;
        isDone = true;
    }


    public void Display(Food food, float size)
    {
        image.sprite = food.FoodImage;
        RectTransform.sizeDelta = Vector2.one * size;
    }
}
