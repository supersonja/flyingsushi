﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trash : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if ((other.attachedRigidbody && other.attachedRigidbody.tag == "food") || other.tag == "food")
        {
            if (other.attachedRigidbody)
            {
                Destroy(other.attachedRigidbody.gameObject);
            }
            else
            {
                Destroy(other.gameObject);
            }
        }
    }
}
