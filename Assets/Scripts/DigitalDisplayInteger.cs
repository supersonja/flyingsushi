﻿using System.Collections;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// A script that displays a integer on a <see cref="TMP_Text"/>.
/// </summary>
public class DigitalDisplayInteger : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The textfield to display the text.")]
    protected TMP_Text display = null;

    [SerializeField, Min(1), Tooltip("The maximum nubers tht can be displayed.")]
    protected byte maxDigits = 12;

    [SerializeField, Min(0f), Tooltip("The time the number gets updated by one.")]
    protected float updateSpeed = 0.001f;

    [SerializeField, Tooltip("The suffix will be added to the end of the number.")]
    protected string suffix = "$";

    [Header("Value Change")] // -------------------------------------------------------------------
    [SerializeField, Tooltip("Called when the value changes from a negative one to a positive one.")]
    protected UnityEvent onPositiveReached = default;
    /// <summary>
    /// Called when the value changes from a negative one to a positive one
    /// </summary>
    public event UnityAction OnPositiveReached
    {
        add => onPositiveReached.AddListener(value);
        remove => onPositiveReached.RemoveListener(value);
    }

    [SerializeField, Tooltip("Called when the value changes from a positive one to a negative one.")]
    protected UnityEvent onNegativeReached = default;
    /// <summary>
    /// Called when the value changes from a positive one to a negative one.
    /// </summary>
    public event UnityAction OnNegativeReached
    {
        add => onNegativeReached.AddListener(value);
        remove => onNegativeReached.RemoveListener(value);
    }


    // --- | Variables | ---------------------------------------------------------------------------------------------------

    protected long currentValue = 0;
    protected long targetValue = 0;
    protected Coroutine timeUpdate = null;
    private byte suffixLength = 0;

    /// <summary>
    /// TRue if the value is currently updating.
    /// </summary>
    public bool IsUpdating => timeUpdate != null;

    /// <summary>
    /// True if the value is positive.
    /// </summary>
    public bool IsPositive { get; protected set; } = true;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    protected virtual void Awake()
    {
        suffixLength = (byte)suffix.Length;
    }

    protected virtual void Start()
    {
        display.SetText(FormatNumber(currentValue));
    }

    /// <summary>
    /// Displays a value.
    /// </summary>
    /// <param name="number">The number to display.</param>
    public virtual void Display(long number)
    {
        targetValue = number;
        if (targetValue != currentValue && !IsUpdating)
        {
            timeUpdate = StartCoroutine(DoUpdateNumber());
        }
    }

    public void Display(int number)
    {
        Display((long)number);
    }
    /// <summary>
    /// Updates the value over time.
    /// </summary>
    protected virtual IEnumerator DoUpdateNumber()
    {
        if (updateSpeed == 0f)
        {
            currentValue = targetValue;
            display.SetText(FormatNumber(currentValue));
            CheckForPositive();
        }
        else
        {
            while (currentValue != targetValue)
            {
                yield return new WaitForSeconds(updateSpeed);
                byte step = 1;
                if (Time.deltaTime > updateSpeed)
                {
                    step = (byte)Mathf.Ceil(Time.deltaTime / updateSpeed);
                }
                if (currentValue < targetValue)
                {
                    if (step > targetValue - currentValue)
                    {
                        step = (byte)(targetValue - currentValue);
                    }
                    currentValue += step;
                }
                else
                {
                    if (step < targetValue - currentValue)
                    {
                        step = (byte)(targetValue - currentValue);
                    }
                    currentValue -= step;
                }
                display.SetText(FormatNumber(currentValue));
                CheckForPositive();
            }
        }
        timeUpdate = null;
    }

    /// <summary>
    /// Checks if the value is positive and updates the <see cref="IsPositive"/> property.
    /// </summary>
    private void CheckForPositive()
    {
        if (IsPositive != currentValue > 0)
        {
            IsPositive = currentValue > 0;
            if (IsPositive)
            {
                onPositiveReached?.Invoke();
            }
            else
            {
                onNegativeReached?.Invoke();
            }
        }
    }

    /// <summary>
    /// Formats the number to a displayable string.
    /// </summary>
    /// <param name="number">The number to format.</param>
    /// <returns>The formatted number.</returns>
    protected virtual string FormatNumber(long number)
    {
        StringBuilder builder = new StringBuilder(number.ToString());
        int offset = number < 0 ? 1 : 0;
        if (builder.Length > maxDigits - suffixLength - offset)
        {
            builder.Remove(offset, builder.Length - offset);
            for (int i = 0; i < maxDigits - suffixLength - offset; i++)
            {
                builder.Append('9');
            }
        }
        for (int i = builder.Length - 3; i > offset; i -= 3)
        {
            builder.Insert(i, ',');
        }
        builder.Append(suffix);
        return builder.ToString();
    }
}
