﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BarColor : MonoBehaviour
{
    [SerializeField]
    private Gradient colors;

    [SerializeField]
    private ColorEvent onColorchanged;

    public void SetProgress (float progress)
    {
        onColorchanged?.Invoke(colors.Evaluate(progress));
    }

    [System.Serializable]
    private class ColorEvent: UnityEvent<Color> { }
}
