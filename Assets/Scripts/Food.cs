﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour , IEquatable<Food>
{
    [SerializeField]
    private Sprite foodImage;

    public Sprite FoodImage
    {
        get
        {
            return foodImage;
        }
    }

    [SerializeField]
    private new string name;

    public string Name
    {
        get
        {
            return name;
        }
    }

    private bool eatable = true;

   

    //set food uneatable
    public void Decay()
    {
        eatable = false;  
    }
    public void SelfDestruct()
    {
        Destroy(gameObject);
    }

    //unity does weird stuff with gameobject IDs so this is a custom comparator
    public bool Equals(Food other)
    {
        return name == other.name;
    }

}
