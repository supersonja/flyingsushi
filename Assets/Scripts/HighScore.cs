﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HighScore : MonoBehaviour
{
    [SerializeField]
    private int maxFails = 3;
    [SerializeField]
    private int baseScore = 30;
    [SerializeField]
    private int scoreMulti = 10;

    [SerializeField]
    private IntEvent onMaxFailsUpdate;
    [SerializeField]
    private IntEvent onHealthUpdate;
    [SerializeField]
    private IntEvent onScoreUpdate;
    [SerializeField]
    private UnityEvent onGameOver;

    private int score = 0;
    private int health;

    private void Awake()
    {
        health = maxFails;
    }

    private void Start()
    {
        onMaxFailsUpdate?.Invoke(maxFails);
    }

    public void LoseHealth()
    {
        health--;
        onHealthUpdate?.Invoke(health);
        if (health <= 0)
        {
            onGameOver.Invoke();
        }
    }

    public void ChangeScore(int points)
    {
        score += baseScore + scoreMulti * scoreMulti;
        onScoreUpdate.Invoke(score);
    }

    [System.Serializable]
    public class IntEvent : UnityEvent<int> { }
}
