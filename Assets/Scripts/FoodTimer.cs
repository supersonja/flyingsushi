﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FoodTimer : MonoBehaviour
{
    [SerializeField]
    private float lifetime = 20;
    [SerializeField]
    private UnityEvent onFoodDie;
    [SerializeField]
    private FloatEvent onTimeUpdate;

    private float currLifetime;

    // Start is called before the first frame update
    void Awake()
    {
        currLifetime = lifetime;
        onTimeUpdate?.Invoke(1f);
    }

    // Update is called once per frame
    void Update()
    {
        currLifetime -= Time.deltaTime;
        if(currLifetime <= 0)
        {
            onTimeUpdate?.Invoke(0f);
            onFoodDie?.Invoke();
            enabled = false;
        }
        else
        {
            onTimeUpdate?.Invoke(currLifetime / lifetime);
        }
    }

    [System.Serializable]
    private class FloatEvent : UnityEvent<float> { }
}
