﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speechbubble : MonoBehaviour
{
    [SerializeField]
    private Canvas mycanvas;

    //bild des essens basically ein image aber food display
    [SerializeField]
    private FoodDisplay display;
    [SerializeField]
    private float imageSize = 0.5f;
    [SerializeField]
    private float spacing = 1f;
    [SerializeField]
    private float topPadding = 0.1f;

    private Dictionary<string, List<FoodDisplay>> displayedFood = new Dictionary<string, List<FoodDisplay>>();

    //will nur bubble sehen wenn person bestellt nicht von anfang an
    private void Awake()
    {
        mycanvas.enabled = false;
    }

    //damit größe gesetzt werden kann
    private RectTransform RectTransform
    {
        get
        {
            return (RectTransform)transform;
        }
    }

    //
    public void DisplayFood(Food[] foods)
    {
        mycanvas.enabled = true;

        // Set panel size.
        RectTransform.sizeDelta = new Vector2(
            spacing * 3f + imageSize * 2f,
            spacing + Mathf.Ceil(foods.Length * 0.5f) * (imageSize + spacing) + topPadding
        );

        for (int i = 0; i < foods.Length; i++)
        {
            Vector2 offset = Vector2.zero;
            if (i % 2 == 1)
            {
                offset.x = 1f;
            }
            else if (i < foods.Length - 1)
            {
                offset.x = -1f;
            }
            offset.y = Mathf.Ceil(foods.Length * 0.5f) - Mathf.Floor(i * 0.5f) - 1f;
            AddFoodItem(foods[i], offset);
        }
    }

    public void UpdateOrder(Food donefood)
    {
        foreach(FoodDisplay dp in displayedFood[donefood.Name])
        {
            if(dp.IsDone == false)
            {
                dp.SetDone();
                return;
            }
        }
        
    }
    
    public void ClearDisplay()
    {

        foreach(KeyValuePair<string,List<FoodDisplay>> entry in displayedFood)
        {
            foreach(FoodDisplay dp in entry.Value)
            {
                Destroy(dp.gameObject);
            }
        }
        displayedFood.Clear();
        mycanvas.enabled = false;
    }

    private void AddFoodItem(Food food, Vector2 offset)
    {
        FoodDisplay newFood = Instantiate(display, RectTransform);
        newFood.Display(food, imageSize);
        newFood.transform.localPosition = offset * new Vector2(0.5f, 1f) * (spacing + imageSize) + Vector2.up * (spacing + imageSize * 0.5f); 
        // Add new food to the displayed food.
        // Check if the food type is already displayed.
        if (!displayedFood.ContainsKey(food.Name))
        {
            // Add a new list for all the food of the same type.
            displayedFood.Add(food.Name, new List<FoodDisplay>());
        }
        // Add the food to the list.
        displayedFood[food.Name].Add(newFood);
    }
}
