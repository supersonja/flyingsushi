﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(Collider), typeof(FixedJoint), typeof(SteamVR_Behaviour_Pose))]
public class Hand : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------
    
    [SerializeField]
    private SteamVR_Action_Boolean grabAction = null;

    [SerializeField]
    private Hand other = null;


    // --- | Variables | ---------------------------------------------------------------------------------------------------

    private Collider hitbox;
    private FixedJoint joint;
    private SteamVR_Behaviour_Pose pose;
    private List<Rigidbody> contacts = new List<Rigidbody>();

    private bool HasContacts
    {
        get
        {
            return contacts.Count > 0;
        }
    }

    private Rigidbody grabbedObject = null;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void Awake()
    {
        hitbox = GetComponent<Collider>();
        joint = GetComponent<FixedJoint>();
        pose = GetComponent<SteamVR_Behaviour_Pose>();
    }

    private void OnTriggerEnter(Collider other)
    {
        // Check if any rigidbodies are touching the hand.
        if (other.attachedRigidbody && !other.attachedRigidbody.isKinematic)
        {
            contacts.Add(other.attachedRigidbody);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // Check if any rigidbodies stopped touching the hand.
        if (other.attachedRigidbody && !other.attachedRigidbody.isKinematic)
        {
            contacts.Remove(other.attachedRigidbody);
        }
    }

    private void Update()
    {
        // Check grab input.
        if (grabAction.GetStateDown(pose.inputSource))
        {
            Pick();
        }
        else if (grabAction.GetStateUp(pose.inputSource))
        {
            Drop(true);
        }
    }

    private void Pick()
    {
        // Check if there are any rigidbodies touching the hand.
        if (HasContacts && !grabbedObject)
        {
            // Get the closest rigidbody.
            grabbedObject = GetClosestContact();
            // Check if the rigidbody is held by the other hand.
            if (other.grabbedObject == grabbedObject)
            {
                other.Drop(false);
            }
            // Connect the rigidbody to the hand.
            joint.connectedBody = grabbedObject;
        }
    }

    private void Drop(bool withVelocity)
    {
        // Check if there is any rigidbody to drop.
        if (grabbedObject)
        {
            // Disconnect the rigidbody from the joint.
            joint.connectedBody = null;
            // Check if the velocity should be updated.
            if (withVelocity)
            {
                // Get the velocity from the pose and apply it to the rigidbidy.
                grabbedObject.velocity = pose.GetVelocity();
                grabbedObject.angularVelocity = pose.GetAngularVelocity();
            }
            // Remove the reference to the rigidbody.
            grabbedObject = null;
        }
    }

    private Rigidbody GetClosestContact()
    {
        try
        {
            // Set the first contact to the closest as default. 
            Rigidbody closest = contacts[0];
            // Calculate the distance to the contact.
            float sqrtDistance = (hitbox.bounds.center - closest.centerOfMass).sqrMagnitude;
            // Loop over all contacts.
            for (int i = 1; i < contacts.Count; i++)
            {
                // Check if the contact at the current index is closer than the saved one.
                if ((hitbox.bounds.center - contacts[i].centerOfMass).sqrMagnitude < sqrtDistance)
                {
                    // Update the closest contact.
                    closest = contacts[i];
                }
            }
            // Return the closest contact.
            return closest;
        }
        catch (IndexOutOfRangeException)
        {
            // Throw an exception if there are no contacts to check.
            throw new IndexOutOfRangeException("The Hand.GetClosestContact method was called without any registered contacts.");
        }
    }
}
