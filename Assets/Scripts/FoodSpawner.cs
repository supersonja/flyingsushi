﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour
{
    //liste alles verschiedenen Food objekte
    [SerializeField]
    private List<GameObject> spawnableFood = new List<GameObject>();
    [SerializeField]
    private float spawntime = 3f;

    private float currentTime = 0;
    [SerializeField]
    private float spawnpointOffset = 1.5f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //zeit wird runtergezählt bis neues Food kommen soll, Spawnfood wird aufgerufen, timer resettet
        if (currentTime <= 0)
        {
            currentTime = spawntime;
            SpawnFood();
        }
        currentTime -= Time.deltaTime;
    }

    //neues objekt vom typ food am spawnpoint erstellen
    private void SpawnFood()
    {
        GameObject fooditem = spawnableFood[Random.Range(0, spawnableFood.Count)];
        Instantiate(fooditem,transform.position - transform.up * spawnpointOffset, Quaternion.identity);
    }


    //visuelle repräsentation des Spawnpoints im viewport
    private void OnDrawGizmosSelected()
    {
        Vector3 spawnpoint = transform.position - transform.up * spawnpointOffset;
        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(spawnpoint, Vector3.right * 0.15f);
        Gizmos.DrawRay(spawnpoint, Vector3.left * 0.15f);
        Gizmos.DrawRay(spawnpoint, Vector3.up * 0.15f);
        Gizmos.DrawRay(spawnpoint, Vector3.down * 0.15f);
        Gizmos.DrawRay(spawnpoint, Vector3.forward * 0.15f);
        Gizmos.DrawRay(spawnpoint, Vector3.back * 0.15f);

    }


}
