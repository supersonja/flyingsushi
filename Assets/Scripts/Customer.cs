﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Customer : MonoBehaviour
{
    [SerializeField]
    private UnityEvent onDisappoint;
    [SerializeField]
    private IntEvent onOrderDone;
    [SerializeField]
    private FoodListEvent onOrder;
    [SerializeField]
    private FoodEvent onFoodEaten;
    [SerializeField]
    private FloatEvent onTimeUpdate;

    private float currOrdertime;
    private float maxOrdertime;
    private bool ordering = false;
    private List<Food> order = new List<Food>();
    private int ordercount = 0;

    private const string FOOD_TAG = "food";

    private void Update()
    {
        //während der Customer eine Bestellung hat läuft die zeit ab, wenn zeit= 0,muss order verworfen werden und negativer effekt passiert
        if (ordering)
        {
            currOrdertime -= Time.deltaTime;
            onTimeUpdate.Invoke(currOrdertime / maxOrdertime);
            if(currOrdertime <= 0)
            {
                ordering = false;
                order.Clear();
                onDisappoint?.Invoke();
            }
        }
    }

    //wenn ein objekt die person trifft muss geprüft werden ob es A food ist und B das Richtige Food
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.attachedRigidbody?.tag == FOOD_TAG || collision.transform.tag == FOOD_TAG)
        {
            Food thefood = collision.collider.attachedRigidbody?.GetComponent<Food>();
            if (!thefood)
            {
                thefood = collision.transform.GetComponent<Food>();
            }
            if (order.Contains(thefood))
            {
                //wenn richtiges food muss das food aus der Bestelliste entfernt werden
                order.Remove(thefood);
                ordercount++;                               //für highscore purposes
                onFoodEaten?.Invoke(thefood);               //in unity gibt speechbubble das food zum wegstreichen
                Destroy(thefood.gameObject);                //das essen muss verschwinden
                if (order.Count == 0)
                {
                    ordering = false;   
                    onOrderDone?.Invoke(ordercount);        //gibt anzahl in Order done für wie viele punkte
                    ordercount = 0;                         //reset everything
                    onTimeUpdate?.Invoke(0);
                }
            }
        }
    }

    //wenn eine Person eine Bestellung beginnt werden die Foods in eine Liste gespeichert, zeit für Bestellung wird berechnet
    public void Order(Food[] order,float time)
    {
        this.order.AddRange(order);
        ordering = true;
        currOrdertime =time + time * (order.Length -1) /2;
        maxOrdertime = currOrdertime;
        onOrder?.Invoke(order);                                 //ruft in unity Speechbubble stuff auf mit liste der Bestellung
        onTimeUpdate?.Invoke(1);
    }

    //unity events mit parametern muss man selber machen 
    [System.Serializable]
    private class IntEvent : UnityEvent<int> { }
    [System.Serializable]
    private class FoodListEvent : UnityEvent<Food[]> { }
    [System.Serializable]
    private class FoodEvent : UnityEvent<Food> { }
    [System.Serializable]
    private class FloatEvent : UnityEvent<float> { }
}
